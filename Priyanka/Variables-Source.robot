*** Variables ***
@{pname}          Pro-ABC4
@{userdetails}    RFUser    21    123456    123456
@{startDate}      Aug 10, 2019 3:30 PM
@{endDate}        Aug 30, 2019 3:30 PM
@{single-couponcode}    TRYSINGLE21    21% discounted offer    5    60    Aug 10, 2019 3:30 PM    Aug 30, 2019 3:30 PM
@{multi-couponcode}    TRYMULTIPLE21    21%discount-multi    Aug 10, 2019 3:30 PM    Aug 30, 2019 3:30 PM
@{combine}        ${single-couponcode}    ${multi-couponcode}
