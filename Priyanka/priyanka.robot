*** Settings ***
Library           SeleniumLibrary
Resource          Variables-Source.robot
Resource          Keywords-Source.robot
Resource          KeywordRitesh.robot
Resource          VariableRitesh.robot

*** Variables ***

*** Test Cases ***
UserReg&EmailVerification
    Open Browser    https://hybrisoob.pragiti.com/electronics/en/login    chrome
    UserRegistration
    Open Browser    https://mailtrap.io/signin    chrome
    EmailVerification

AddtoCart&BOEntry
    Open Browser    https://hybrisoob.pragiti.com/electronics/en/login    chrome
    Maximize Browser Window
    Login Storefront
    AddProductToCart
    ${cardid}    Get Text    class=cart__id    #extract the cart id from the cart page
    Open Browser    https://hybrisoob.pragiti.com/backoffice/login.zul    chrome
    LoginBackOffice
    VerifyBO
    Input Text    class=z-bandbox-input    ${cardid}    #search for the cart id in BO
    Click Button    class=yw-textsearch-searchbutton
    sleep    5s

CreateProduct&SearchFE
    Open Browser    https://b2caccelerator.pragiti.com/backoffice/login.zul    chrome
    Maximize browser
    LoginBackOffice
    CreateProduct
    search product backoffice
    ProductSync
    FullIndexing

*** Keywords ***
