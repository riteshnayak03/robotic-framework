*** Settings ***
Suite Setup
Library           SeleniumLibrary

*** Variables ***
@{enterproduct}    pro-abc
@{price}          100
@{stock}          34    1
@{RuleManagement}    electronicsPromoGrp    8002    Jan 09, 2019 10:35 AM    Dec 09, 2020 10:35 PM    482105    USD    10

*** Test Cases ***
Indexing TC
    [Setup]    openbrowser    https://hybrisoob.pragiti.com/backoffice/    chrome
    Maximize browser
    Sleep    2s
    Backoffice login
    Facet search configuration
    Close Browser

Update Price TC
    [Setup]    openbrowser    https://hybrisoob.pragiti.com/backoffice/login.zul    Chrome
    Backoffice login
    Update Price
    Close Browser

Stock Update
    [Setup]    Open browser    https://hybrisoob.pragiti.com/backoffice/login.zul    Chrome
    Backoffice login
    Stock Update
    Close Browser

Promotion Product fixed discount
    [Setup]    Open Browser    https://hybrisoob.pragiti.com/backoffice/    Chrome
    Backoffice login
    #Publish Template
    Sleep    2s
    promotion rules
    Sleep    10s
    promotion config
    Sleep    10s
    #Publish Promotion
    Sleep    10s
    Verify promotion frontend

practise
    [Setup]    Open Browser    https://hybrisoob.pragiti.com/electronics/en/    Chrome
    Maximize browser
    Open Browser    https://hybrisoob.pragiti.com/electronics/en/cart    Chrome
    Sleep    5s
    Input Text    id=js-site-search-input    @{RuleManagement}[4]
    sleep    5s
    Mouse Over    xpath=//div[@class='name']
    Sleep    5s
    Click Element    xpath=//div[@class='name']
    Sleep    5s
    Click Element    id=addToCartButton
    Sleep    5s
    Click Element    xpath=//*[@id="addToCartLayer"]/a[1]
    sleep    2s
    Execute Javascript    $(document).scrollTop(${600})

*** Keywords ***
Add product from PLP
    Click Button    xpath=//*[@id="addToCartForm3514519"]/button

Maximize browser
    Maximize Browser Window

Backoffice login
    Maximize browser
    Click Element    xpath=//*[@name="j_username"]
    Sleep    2s
    Input Text    xpath=//*[@name="j_username"]    admin
    Click Element    xpath=//*[@name="j_password"]
    Sleep    2s
    Input Text    xpath=//*[@name="j_password"]    nimda
    Click Button    xpath=//*[@type="submit"]
    Sleep    5s

search product backoffice
    Input Text    xpath=/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/input[1]    @{enterproduct}

Product Price
    Input Text    xpath=/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]    @{price}

Currency
    Click Element    xpath=/html[1]/body[1]/div[5]//div[1]/div[4]/div[1]//div[2]/span[1]
    Sleep    10s
    Click Element    xpath=(//td[@class='z-listcell'])[6]

Unit
    Click Element    xpath=(//div[@class='ye-default-reference-editor ye-com_hybris_cockpitng_editor_defaultreferenceeditor z-div'])[7]
    Sleep    2s
    Click Element    xpath=//div[@class='z-bandbox-popup z-bandbox-open z-bandbox-shadow']

Select In Stock
    Click Element    xpath=(//span[@class='z-comboitem-text'])[4]    #Enter 1 for na in [1] \ #Enter 2 for Force in Stock [2] #Enter 3 for Force out of stock [3] #Enter 4 for Not specified [4]

Available amount
    Input Text    xpath=(//*[@type="text"])[42]    @{stock}[1]
    Sleep    2s
    Input Text    xpath=(//*[@type="text"])[41]    @{stock}[0]

Create promotion rule
    Clear Element Text    xpath=//input[@class='z-textbox']
    Sleep    2s
    Input Text    xpath=//input[@class='z-textbox']    product_fixed_discount_C
    Sleep    10s

promotion config
    Input text    xpath=(//input[@class='z-bandbox-input'])[3]    @{RuleManagement}[0]    #Enters website name in the field
    Sleep    10s
    Click Element    xpath=/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/table[2]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/div[2]/div[2]/span[1]/a[1]/i[1]    \    #Select website
    Sleep    10s
    Click Element    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[3]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/span[1]    \    #Select website
    Sleep    10s
    Click Element    //button[@class='select-button y-btn-primary z-button']    \    #clicks Select button
    Sleep    10s
    Click Element    xpath=(//input[@type="text"])[22]
    Sleep    5s
    Clear Element Text    xpath=(//input[@type="text"])[22]
    Sleep    5s
    Input Text    xpath=(//input[@type="text"])[22]    @{RuleManagement}[1]
    Sleep    10s
    Input text    xpath=//input[@class='z-datebox-input']    @{RuleManagement}[2]    #enters start date
    Sleep    5s
    Input text    xpath=(//input[@class='z-datebox-input'])[2]    @{RuleManagement}[3]    #enters end date
    Sleep    2s
    Click Element    xpath=//*[@title="Conditions & Actions"]    \    #CLicks next tab
    Sleep    10s
    Click Element    xpath=(//div[@class='yrb-condition-caption-content'])[2]    \    #clicks on qualifying products
    Sleep    5s
    Input Text    xpath=(//input[@class='z-bandbox-input'])[7]    @{RuleManagement}[4]
    Sleep    10s
    Click Element    xpath=/html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/span[1]    \    #clicks on staged product
    Sleep    2s
    Click Element    xpath=//div[@class='yrb-action-caption-content']    #clicks action
    Sleep    2s
    Click Element    xpath=//div[@class='z-listfooter-content']
    Sleep    5s
    Input Text    xpath=(//input[@class='z-bandbox-input'])[8]    @{RuleManagement}[5]    #Enters currency format
    Sleep    5s
    Click Element    xpath=/html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]    \    #Selects USD
    Sleep    5s
    Input Text    xpath=//input[@class='ye-com_hybris_cockpitng_editor_defaultbigdecimal z-decimalbox']    @{RuleManagement}[6]    #Enters discount value
    Sleep    10s
    Click element    xpath=/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[1]/div[3]/img[2]    \    #saves currency and discount
    Sleep    5s
    Click Element    xpath=(//button[text()='Save'])[1]    \    #Clicks on save button
    Sleep    5s

Publish Promotion
    Sleep    5s
    Click Element    xpath=//*[@title="Marketing"]
    Sleep    10s
    Click Element    xpath=//*[@title="Publish Promotion Rule For Module"]
    Sleep    10s
    Click Element    xpath=(//input[@class='z-combobox-input'])[13]
    Sleep    10s
    Click Element    xpath=(//li[@class='z-comboitem'])[2]
    Sleep    10s
    Click Element    xpath=//button[@class='yw-btn-primary z-button']

Publish Template
    Sleep    5s
    Click Element    xpath=//*[@title="Marketing"]
    Sleep    5s
    Click Element    xpath=//*[@title="Promotion Templates"]
    Sleep    5s
    Click Element    xpath=(//span[@class='yw-listview-cell-label z-label'])[29]    #selects product fixed discount
    Sleep    5s
    Click Element    xpath=//*[@title="Create a promotion rule using this template"]    #Clicks on create peomotion rule link
    Sleep    5s
    Create promotion rule
    Click Element    xpath=//button[@class='yw-btn-primary z-button']    #clicks Ok Button from pop up

Verify promotion frontend
    Maximize browser
    Open Browser    https://hybrisoob.pragiti.com/electronics/en/cart    Chrome
    Sleep    5s
    Input Text    id=js-site-search-input    @{RuleManagement}[4]
    sleep    5s
    Mouse Over    xpath=//div[@class='name']
    Sleep    5s
    Click Element    xpath=//div[@class='name']
    Sleep    5s
    Click Element    id=addToCartButton
    Sleep    5s
    Click Element    xpath=//*[@id="addToCartLayer"]/a[1]
    sleep    2s
    Execute Javascript    $(document).scrollTop(${600})

Facet search configuration
    Click element    xpath=//*[@title="System"]
    Sleep    5s
    Click element    xpath=//*[@title="Search and Navigation"]
    Sleep    5s
    Click element    xpath=//*[@title="Indexed Types"]
    Sleep    5s
    Click element    xpath=//*[@title="Facet Search Configurations"]
    Sleep    5s
    Click element    xpath=/html[1]/body[1]//div[2]/div[3]//table[1]/tbody[1]/tr[4]/td[1]/div[1]/span[2]
    Sleep    2s
    Click element    xpath=/html[1]/body[1]//div[1]//div[3]/div[1]//div[2]//table[1]/tbody[1]/tr[1]/td[1]//td[3]/div[1]//tr[1]/td[3]/div[1]    #Clicks on Index link
    Sleep    5s
    Click Element    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]    #clicks outside the pop up window
    Sleep    5s
    Click Button    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[1]    #clicks on start button
    Sleep    20s

promotion rules
    Click Element    xpath=//*[@title="Marketing"]
    Sleep    5s
    Click Element    xpath=//*[@title="Promotion Rules"]    #click on promotion rules
    Sleep    10s
    Click Element    xpath=//input[@class='z-bandbox-input z-bandbox-rightedge']    #click search box
    Sleep    5s
    Input Text    xpath=//input[@class='z-bandbox-input z-bandbox-rightedge']    product_fixed_discount_C
    Sleep    5s
    Click Element    xpath=//button[@class='yw-textsearch-searchbutton y-btn-primary z-button']
    Sleep    5s
    Click Element    xpath=(//span[@class='yw-listview-cell-label z-label'])[2]    #clicks on search result

Update Price
    Sleep    5s
    Click element    xpath=//*[@title="Catalog"]    #Selects Catalog from the list
    Sleep    5s
    Click element    xpath=//*[@title="Products"]
    sleep    2s
    Click Element    xpath=(//*[@type="text"])[3]
    search product backoffice
    Sleep    10s
    click element    xpath=/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/button[1]
    Sleep    5s
    Click Element    xpath=/html[1]/body[1]//div[3]/div[2]/div[1]/div[2]/div[1]/div[3]/table[1]/tbody[1]/tr[1]
    sleep    10s
    Click Element    xpath=/html[1]/body[1]//div[1]//div[2]//div[4]//ul[1]/li[4]/span[1]//span[1]
    sleep    10s
    Click Element    xpath=/html[1]/body[1]/div[1]//div[2]//div[4]/div[1]//table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[2]/div[2]/div[2]/span[1]
    Sleep    5s
    Product Price
    Sleep    5s
    Currency
    Sleep    5s
    Unit
    Sleep    5s
    Click Element    xpath=/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[2]/div[3]/button[1]    #Clicks on done Button
    Sleep    5s
    Click Element    xpath=/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[2]/div[3]/div[3]/button[1]    #Click on save button
    Sleep    5s

Stock Update
    Sleep    2s
    Click element    xpath=//*[@title="Catalog"]
    Sleep    5s
    Click element    xpath=//*[@title="Products"]
    sleep    2s
    Click Element    xpath=(//*[@type="text"])[3]
    search product backoffice
    click element    xpath=/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/button[1]
    Sleep    5s
    Click Element    xpath=/html[1]/body[1]//div[3]/div[2]/div[1]/div[2]/div[1]/div[3]/table[1]/tbody[1]/tr[1]
    Sleep    5s
    Click Element    xpath=//*[@title="Stock"]    #Clicks on Stock category
    Sleep    5s
    Execute Javascript    $(document).scrollTop(${1200})
    Click Button    xpath=/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[10]/div[1]/div[3]/div[2]/table[2]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/div[2]/div[2]/button[1]    #clicks on Find all stock button
    Sleep    5s
    Click Element    xpath=//div[@class='ya-com_hybris_cockpitng_action_create cng-action cng-action-enabled z-div']    #Clicks on + button
    Sleep    5s
    Input Text    xpath=/html[1]/body[1]/div[7]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/span[1]/input[1]    warehouse_w Warehouse
    Sleep    5s
    Click Element    xpath=//span[text()='warehouse_w Warehouse West']    #selects warehouse from dropdown
    Sleep    5s
    Sleep    2s
    Click Element    xpath=//tr[@class='yw-coll-browser-hyperlink z-listitem']
    Sleep    2s
    Click Element    xpath=(//a[@class='z-combobox-button'])[11]
    Sleep    5s
    Select in Stock
    Sleep    5s
    Available amount
    Sleep    5s
    Click Element    xpath=/html[1]/body[1]/div[6]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[2]/div[3]/div[3]/button[1]    #clicks on save button.
    Sleep    10s
