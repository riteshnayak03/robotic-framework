*** Settings ***
Suite Setup
Library           SeleniumLibrary
Resource          TestR.txt

*** Variables ***
@{UN}             admin
@{PWD}            nimda
@{PromotionConfigurationDetails}    electronicsPromoGrp    4000    Jul 01, 2019 6:03 AM    Dec 31, 2019 6:37 AM    489702    30

*** Test Cases ***
TC_01
    [Tags]    BakOffice
    [Setup]    open Browser    https://hybrisoob.pragiti.com/backoffice/login.zul    chrome
    EnterCredentials
    ClickOnUserTab
    sleep    5s
    Click On Employee Tab
    sleep    10s
    ClickOnCreateNewEmpIcon
    Import Resource

testing
    [Setup]    open Browser    https://hybrisoob.pragiti.com/backoffice/login.zul    chrome
    EnterCredentials
    click element    xpath=(//*[@type="button"])[8]
    Sleep    5s
    Click Element    xpath=/html[1]/body[1]//div[3]/div[2]/div[1]/div[2]/div[1]/div[3]/table[1]/tbody[1]/tr[1]
    sleep    10s
    Click Element    xpath=/html[1]/body[1]//div[1]//div[2]//div[4]//ul[1]/li[4]/span[1]//span[1]
    sleep    10s
    Click Element    xpath=/html[1]/body[1]/div[1]//div[2]//div[4]/div[1]//table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[2]/div[2]/div[2]/span[1]
    Sleep    5s
    Input Text    xpath=/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]    100
    Sleep    5s
    Click Element    xpath=/html[1]/body[1]/div[5]//div[1]/div[4]/div[1]//div[2]/span[1]
    Sleep    5s

TC_2
    [Tags]    Fixed_Price_Promotion
    [Setup]    open Browser    https://hybrisoob.pragiti.com/backoffice/login.zul    chrome
    Maximize Browser Window
    EnterCredentials
    SearchEnterTextbackoffice
    sleep    5s
    #CreatePromotionTemplate
    sleep    10s
    click element    xpath=//span[text()='Promotion Rules']    \    #click on promotion rule link
    sleep    5s
    click element    xpath=//span[text()='product_fixed_price for automation']
    sleep    5s
    PromotionRuleProperties
    sleep    5s
    ConditionsAndAction(promotion)
    PublishPromotion

BacOff
    [Setup]    open Browser    https://hybrisoob.pragiti.com/backoffice/login.zul    chrome
    Maximize Browser Window
    EnterCredentials
    click element    xpath=/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[7]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/img[1]

*** Keywords ***
EnterCredentials
    input text    xpath=//*[@name="j_username"]    @{UN}
    input text    xpath=//*[@name="j_password"]    @{PWD}
    click button    xpath=//*[@type="submit"]

ClickOnCreateNewEmpIcon
    sleep    2s
    click element    xpath=//div[@class='yw-nested-widget widget_cnt']
    sleep    2s
    click element    xpath=(//div[@class='yw-listview-actioncontainer z-div'])[1]
    sleep    2s
    click element    xpath=(//div[@class='yw-listview-actionSlot z-div'])[1]
    sleep    2s
    click element    xpath=(//table[@class='cng-action-group cng-action-separator z-hbox'])[5]
    sleep    2s
    click element    xpath=(//div[@class='ya-create-container z-div'])[1]
    sleep    2s
    input text    xpath=(//*[@type='text'])[23]    1234
    sleep    2s
    click element    xpath=/html[1]/body[1]/div[4]/div[2]//div[4]//div[2]//input[1]
    sleep    2s
    input text    xpath=/html[1]/body[1]/div[4]/div[2]//div[4]//div[2]//input[1]    xxx
    sleep    2s
    input text    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/input[1]    Product is good
    sleep    2s
    click button    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/button[1]
    sleep    5s
    click element    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/span[1]/input[1]
    sleep    5s
    click element    xpath=/html[1]/body[1]/div[5]/div[1]/div[1]/div[2]/ul[1]/li[5]/a[1]
    sleep    2s
    click element    xpath=/html[1]/body[1]/div[5]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/span[1]
    sleep    5s
    click element    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/span[1]/input[1]
    sleep    5s
    click element    xpath=/html[1]/body[1]/div[5]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/span[1]
    sleep    5s
    click element    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]
    sleep    5s
    click element    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]

ClickOnUserTab
    sleep    2s
    click element    xpath=(//div[@class='yw-treeCellInner z-div'])[6]

Click On Employee Tab
    sleep    2s
    click element    xpath=//*[@title='Employees']
    click element    xpath=//*[@title='Employees']
    sleep    2s

SearchEnterTextbackoffice
    click element    xpath=//input[@class='yw-explorerTree-filterTextbox yw-filter-textbox y-general-textinput z-textbox']    #click on search button
    input text    xpath=//input[@class='yw-explorerTree-filterTextbox yw-filter-textbox y-general-textinput z-textbox']    promotion    #enter \ text promotion

CreatePromotionTemplate
    click element    xpath=//span[text()='Promotion Templates']    \    #click on template tab
    sleep    5s
    click element    xpath=//div[@class='yw-search-mode-container z-div']    \    #click on search icon of main search field
    sleep    5s
    input text    xpath=//input[@class='z-bandbox-input z-bandbox-rightedge']    product_fixed_price    #Enter text in search field
    sleep    3s
    click button    xpath=//button[@class='yw-textsearch-searchbutton y-btn-primary z-button']    \    #click on go button
    sleep    3s
    click element    xpath=//td[@class='yw-listview-cell, yw-listview-cell-fill z-listcell']    \    #select the promotion type
    sleep    5s
    click element    xpath=//td//img[@title='Create a promotion rule using this template']    \    #click on icon create new promotion rule
    sleep    2s
    Clear Element Text    xpath=//input[@class='z-textbox']    #clears the existing promotion name form field
    sleep    5s
    PromotionRuleName
    sleep    5s
    click button    xpath=//button[@class='yw-btn-primary z-button']    \    #click ok button

PromotionRuleName
    input text    xpath=//input[@class='z-textbox']    product_fixed_price for automation    #select the promotion type from promotion rule list.

PromotionRuleProperties
    click button    xpath=(//button[@class='yw-expandCollapse z-button'])[1]    \    #Click on expand link
    sleep    5s
    click element    xpath=(//input[@class='z-bandbox-input'])[3]    \    #click on website field
    sleep    5s
    input text    xpath=(//input[@class='z-bandbox-input'])[3]    @{PromotionConfigurationDetails}[0]    #select website
    sleep    5s
    click element    xpath=/html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/span[1]    #select website from popup
    sleep    5s
    input text    xpath=(//input[@class='ye-com_hybris_cockpitng_editor_defaultinteger z-longbox'])[1]    @{PromotionConfigurationDetails}[1]    #enter priority
    sleep    5s
    input text    xpath=(//input[@class='z-datebox-input'])[1]    @{PromotionConfigurationDetails}[2]    #enter start date
    sleep    5s
    input text    xpath=(//input[@class='z-datebox-input'])[2]    @{PromotionConfigurationDetails}[3]    #enter end date

ConditionsAndAction(promotion)
    click element    xpath=//span[text()='Conditions & Actions']    #click conditions and actions
    #click button    xpath=(//button[@class='yrb-delete-btn ye-delete-btn'])[3]    #Remove Categories \ link
    sleep    5s
    click element    xpath=//span[text()='Qualifying products']    #click qualifying products
    sleep    5s
    input text    xpath=(//input[@class='z-bandbox-input'])[7]    @{PromotionConfigurationDetails}[4]    #enter product id to select in product field
    #click element    xpath=//div[@class='ye-default-reference-editor-listbox z-listbox']    #click pop-up
    click element    xpath=/html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/span[1]    #select staged vesrion product
    sleep    5s
    #click element    xpath=(//div[@class='ye-default-reference-editor-remove-button ye-delete-btn z-div'])[2]    #remove selected staged version product
    #sleep    2s
    #click element    xpath=//div[@class='yrb-action-caption']
    #sleep    5s
    #click button    xpath=(//div[@class='ye-save-container z-div'])[1]
    #click element    xpath=(//span[text()='Drag and Drop Conditions Here'])[1]
    click element    xpath=//span[text()='Target fixed price on products']    #click on target fixed price on products.
    sleep    5s
    click element    xpath=//span[text()='Add new item']    #click on add new item
    sleep    5s
    click element    xpath=(//input[@class='z-bandbox-input'])[8]
    sleep    5s
    click element    xpath=/html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]
    sleep    5s
    click element    xpath=//input[@class='ye-com_hybris_cockpitng_editor_defaultbigdecimal z-decimalbox']
    sleep    5s
    input text    xpath=//input[@class='ye-com_hybris_cockpitng_editor_defaultbigdecimal z-decimalbox']    @{PromotionConfigurationDetails}[5]
    sleep    5s
    click element    xpath=/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[1]/div[3]/img[2]
    sleep    5s
    click button    xpath=(//button[text()='Save'])[1]

PublishPromotion
    click element    xpath=//img[@title='Publish Promotion Rule For Module']    #click on publish
    sleep    10s
    click element    xpath=/html[1]/body[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/span[2]/input[1]
    sleep    10s
    click element    (//span[@class='z-comboitem-text'])[3]    #select the type to publish promotion.
    sleep    5s
    click button    xpath=//button[text()='OK']
