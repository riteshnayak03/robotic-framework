*** Settings ***
Suite Setup       open browser    https://hybrisoob.pragiti.com/electronics/en/login    chrome
Library           SeleniumLibrary
Resource          TestR.txt
Resource          r1.robot

*** Variables ***
@{Enter_UN}       user1@gmail.com
@{Enter_PWD}      123456

*** Test Cases ***
TC_01(PLP)
    [Tags]    PLP
    Maximize Browser Window
    EnterUN and PWD
    add product to cart
    SortByFunctionality
    ScrollPage
    Click Element    xpath=//div[@id='product-facet']
    VerifyLocationFunctionlity
    sleep    10s
    Execute Javascript    $(document).scrollTop(${500})
    sleep    2s

TC_2(Search)
    [Tags]    SLP
    [Setup]    open browser    https://hybrisoob.pragiti.com/electronics/en/login    chrome
    sleep    2s
    Maximize Browser Window
    sleep    2s
    EnterUN and PWD
    sleep    2s
    SearchFunctionality
    sleep    2s
    VerifyPageNvaigationlink
    sleep    2s
    ScrollPage
    sleep    2s
    AddProductFromSLP
    sleep    2s
    SortByFunctionality
    sleep    2s
    VerifyLocationFunctionlity
    sleep    2s
    VerifyShopByBrand
    sleep    2s
    ClickHomeLink
    sleep    2s
    SignOut
    sleep    2s
    Close Browser

TC_3
    [Documentation]    This test case is to verify the \ PDP
    [Tags]    PDP
    [Setup]    open browser    https://hybrisoob.pragiti.com/electronics/en/login    chrome
    Maximize Browser Window    #Opens browser Enter URL and mximize the window
    EnterUN and PWD    #Enter username and pwd
    NavigateToPDP    #click on primary category and navigate to PDP
    sleep    2s
    AddQty    #add qty in the qty field of product
    scrollPage
    sleep    2s
    click link    xpath=//a[contains(@class,'js-writeReviewTab')]    #click review tab
    sleep    5s
    input text    id=review.headline    xyz    #Input the value
    sleep    5s
    input text    id=review.comment    The product quality is good    #input value in Comment box
    sleep    5s
    click button    xpath=(//button[@type='submit'])[5]    \    #Click on submit button
    sleep    2s
    click link    xpath=//li[@id='accessibletabsnavigation0-3']//a[contains(text(),'Delivery')]    \    #Click on the tab delivery
    sleep    2s
    click link    xpath=//li[@id='accessibletabsnavigation0-0']//a[contains(text(),'Product Details')]    \    #click on the tab product details
    sleep    2s
    click image    xpath=//div[contains(@class,'carousel gallery-carousel js-gallery-carousel hidden-xs hidden-sm owl-carousel owl-theme')]//div[2]//a[1]//img[1]    \    #click on image
    sleep    2s
    click button    id=addToCartButton    \    #add product to cart
    Go back
    sleep    5s
    close browser

Testing
    Maximize Browser Window
    EnterUN and PWD
    ScrollPage
    add product to cart
    sleep    5s
    Close Browser
    Open Browser    https://hybrisoob.pragiti.com/backoffice/    chrome
    sleep    5s

*** Keywords ***
add product to cart
    [Documentation]    Navigate to home page add product to cart
    click link    xpath=//a[.//text()='Film Cameras']
    click button    xpath=(//button[@type='submit'])[3]

SortByfunctionality
    [Documentation]    To test sort functionality
    Select From List    id=sortOptions1    Top Rated
    Select From List    id=sortOptions1    relevance
    Select From List    id=sortOptions1    Name (ascending)
    Select From List    id=sortOptions1    Name (descending)
    Select From List    id=sortOptions1    Price (lowest first)

EnterUN and PWD
    [Documentation]    To enter username and password multiple value can be entered
    input text    id=j_username    @{Enter_UN}
    input password    id=j_password    @{Enter_PWD}
    click button    xpath=//button[@class='btn btn-primary btn-block']

SetupBrowser
    open browser    https://hybrisoob.pragiti.com    chrome

TestShopByPrice
    click button    //body[contains(@class,'language-en')]/main/div[@class='main__inner-wrapper']/div[@class='row']/div[@class='col-xs-3']/div[@class='yCmsContentSlot search-list-page-left-refinements-slot']/div[@class='yCmsComponent search-list-page-left-refinements-component']/div[@id='product-facet']/div[2]/div[2]/ul[1]/li[1]/form[1]/label[1]/span[1]/span[1]

VerifyPageNvaigationlink
    click link    xpath=//div[@class='pagination-bar top']//a[@class='glyphicon glyphicon-chevron-right']
    click link    xpath=//div[@class='pagination-bar top']//a[@class='glyphicon glyphicon-chevron-right']
    click link    xpath=//div[@class='pagination-bar top']//a[@class='glyphicon glyphicon-chevron-right']
    click link    xpath=//div[@class='pagination-bar top']//a[@class='glyphicon glyphicon-chevron-left']
    click link    xpath=//div[@class='pagination-bar top']//a[@class='glyphicon glyphicon-chevron-left']

AddProductFromSLP
    click button    xpath=(//button[@type='submit'])[3]

SearchFunctionality
    [Tags]    MainSearch
    input text    id=js-site-search-input    FUN Flash Single Use Camera, 27+12 pic
    click button    xpath=(//button[@type='submit'])[1]

ScrollPage
    Execute Javascript    $(document).scrollTop(${500})

VerifyLocationFunctionlity
    click button    id=product_7798410
    input text    id=locationForSearch    Fukuoka Hotel Monterey La Soeur Fukuoka
    click button    id=cboxClose

VerifyShopByBrand
    click link    xpath=//span[@class='facet__text']//a[contains(text(),'Sony')]
    Go Back
    click link    xpath=//span[@class='facet__text']//a[contains(text(),'Canon')]
    Go Back

ClickHomeLink
    click link    xpath=//a[contains(text(),'Home')]

signout
    click link    xpath=//li[@class='liOffcanvas']//a[contains(text(),'Sign Out')]

NavigateToPDP
    click link    xpath=//a[.//text()='Film Cameras']
    click link    xpath=//a[contains(text(),'FUN Flash Single Use Camera, 27+12 pic')]

AddQty
    input text    id=pdpAddtoCartInput    3
    click button    xpath=(//button[@type='button'])[7]
    click button    xpath=(//button[@type='button'])[6]
